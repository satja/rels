import sys
import time
import numpy as np

def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        if result is None:
            return None
        te = time.time()
        if not isinstance(result, tuple):  # single task
            print(result, te - ts, -1)
        elif len(result) > 3:
            print(result[0], te - ts, np.mean(result[1]),
                    result[2], '--',
                    ' '.join(map(str, result[3])), '--',
                    ' '.join(map(str, result[4])))
        else:
            print(result[0], te - ts, np.mean(result[1]))
        return result
    return timed
