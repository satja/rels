from random import randint, randrange, uniform, shuffle
import sys
import pickle
import numpy as np
import clus_mip
import mip
import greedy
import mip_greedy
import single_ap
import proposed
from copy import deepcopy
from math import log

ITERATIONS = 200
WSDREAM_USERS = 339
WSDREAM_SERVICES = 5825
WSDREAM_RT = 'rtMatrix.txt'
WSDREAM_TP = 'tpMatrix.txt'

tmp = []

def single_class_tests():
    with open('single.dat', 'wb') as f:
        test = test_case(1, 500, 1, 100, 1, 50)
        pickle.dump(test, f)
        test = test_case(1, 500, 1, 100, 50, 1000)
        pickle.dump(test, f)

def single_class_vary():
    with open('single_vary.dat', 'wb') as f:
        for users in range(50, 501, 50):
            test = test_case(1, users, 1, 100, 1, 50)
            pickle.dump(test, f)
            test = test_case(1, users, 1, 100, 50, 1000)
            pickle.dump(test, f)
        for services in range(40, 201, 20):
            test = test_case(1, 300, 1, services, 1, 50)
            pickle.dump(test, f)
            test = test_case(1, 300, 1, services, 50, 1000)
            pickle.dump(test, f)

def eq_qos_tests():
    with open('eq_qos.dat', 'wb') as f:
        for diff in range(3):
            test = test_case(diff, 50, 4, 200, True)
            pickle.dump(test, f)

def general_tests():
    with open('test.dat', 'wb') as f:
        for diff in range(3):
            test = test_case(diff, 100, 8, 300)
            pickle.dump(test, f)

def general_vary():
    with open('general_vary.dat', 'wb') as f:
        for users in range(20, 201, 20):
            test = test_case(1, users, 8, 200)
            pickle.dump(test, f)
        for services in range(80, 401, 40):
            test = test_case(1, 100, 4, services)
            pickle.dump(test, f)
        for classes in range(2, 11):
            test = test_case(1, 100, classes, 200)
            pickle.dump(test, f)

def generate_qos(num_users, service_indices, qos_type):
    ret = np.zeros((num_users, len(service_indices)))
    if qos_type == 'availability':
        for u in range(num_users):
            for i, _ in enumerate(service_indices):
                ret[u, i] = log(1 - 0.1 ** max(np.random.normal(2, .5), 1))
    elif qos_type == 'price':
        for u in range(num_users):
            for i, _ in enumerate(service_indices):
                ret[u, i] = max(0, uniform(-1, 4))
    else:
        # response time
        with open(qos_type, 'r') as f:
            for u, line in enumerate(f):
                if u == num_users: break
                vals = list(map(float, line.split()))
                for i, x in enumerate(service_indices):
                    ret[u, i] = vals[x]
        ret[np.where(ret == -1)] = 20
    return ret

def avg_tp():
    tpSum = np.zeros(WSDREAM_SERVICES)
    with open(WSDREAM_TP, 'r') as f:
        for u, line in enumerate(f):
            for i, x in enumerate(line.split()):
                tpSum[i] += float(x)
    return tpSum / WSDREAM_USERS

def test_case(difficulty, num_users, num_classes, num_services, tp_min=0, tp_max=1000,
        eq_qos=False, qos_dimensions=3):
    tp_avg = avg_tp()

    services = set()
    while len(services) < num_services:
        i = randrange(WSDREAM_SERVICES)
        if tp_min <= tp_avg[i] <= tp_max and i not in services:
            services.add(i)
    services = sorted(list(services))
    classes = [(i, randrange(num_classes)) for i in services]

    qos = [[None for d in range(qos_dimensions)] for c in range(num_classes)]
    tp = [None for c in range(num_classes)]

    for c in range(num_classes):
        service_indices = [i for i, x in classes if x == c]
        qos[c][0] = -generate_qos(num_users, service_indices, 'availability')
        qos[c][1] = generate_qos(num_users, service_indices, 'price')
        qos[c][2] = generate_qos(num_users, service_indices, WSDREAM_RT)
        tp[c] = np.array(list(map(int, tp_avg[service_indices])))
        global tmp
        tmp += [np.mean(tp[c])]
        #tp[c][np.where(tp[c] <= 0)] = 1

    if num_classes == 1:
        user_class_demand = [[1 for c in range(num_classes)] for u in range(num_users)]
    else:
        user_class_demand = [[randrange(2) for c in range(num_classes)]\
                for u in range(num_users)]

    user_reqs = np.array([[0.0 for d in range(qos_dimensions)] for u in range(num_users)])
    for d in range(qos_dimensions):
        for c in range(num_classes):
            for u in range(num_users):
                user_reqs[u][d] += user_class_demand[u][c] *\
                        np.percentile(qos[c][d][u], (80, 60, 40)[difficulty])

    if eq_qos:
        for c in range(num_classes):
            for d in range(qos_dimensions):
                u = randrange(num_users)
                for r in range(num_users):
                    qos[c][d][r] = qos[c][d][u]
    return num_users, num_classes, qos_dimensions, user_reqs, user_class_demand, qos, tp

def run(filename):
    tests = []
    with open(filename, 'rb') as f:
        while True:
            try:
                test = pickle.load(f)
                tests.append(test)
            except EOFError:
                return tests

if __name__ == '__main__':
    for it in range(ITERATIONS):
        print(it, file=sys.stderr)

        if sys.argv[1] in ('1', '3'):
            single_class_tests()

        if len(sys.argv) > 2 and sys.argv[2] in ('1', '3'):
            eq_qos_tests()

        if len(sys.argv) > 3 and sys.argv[3] in ('1', '3'):
            general_tests()

        if len(sys.argv) > 4 and sys.argv[4] in ('1', '3'):
            single_class_vary()

        if len(sys.argv) > 5 and sys.argv[5] in ('1', '3'):
            general_vary()

        if sys.argv[1] in ('2', '3'):
            for test in run('single.dat'):
                if mip.solve(deepcopy(test)) is None:
                    break
                single_ap.solve(deepcopy(test))
                proposed.solve(deepcopy(test), False)
                proposed.solve(deepcopy(test), True)
                print()
            print('---')

        if len(sys.argv) > 2 and sys.argv[2] in ('2', '3'):
            for test in run('eq_qos.dat'):
                if mip.solve(deepcopy(test)) is None:
                    break
                if clus_mip.solve(deepcopy(test), num_clusters=2) is None:
                    break
                if clus_mip.solve(deepcopy(test), num_clusters=3) is None:
                    break
                proposed.solve(deepcopy(test), False)
                proposed.solve(deepcopy(test), True)
                print()
            print('---')

        if len(sys.argv) > 3 and sys.argv[3] in ('2', '3'):
            for test in run('test.dat'):
                mip.solve(deepcopy(test))
                proposed.solve(deepcopy(test), False)
                proposed.solve(deepcopy(test), True)
                #greedy.solve(deepcopy(test))
                mip_greedy.solve(deepcopy(test))
                print()
            print('---')

        if len(sys.argv) > 4 and sys.argv[4] in ('2', '3'):
            for it, test in enumerate(run('single_vary.dat')):
                m, n, qos_dims, user_reqs, _, qos, tp = test
                print(m, 1, len(tp[0]), it % 2)
                single_ap.solve(deepcopy(test))
                proposed.solve(deepcopy(test), False)
                proposed.solve(deepcopy(test), True)
                print()
            print('---')

        if len(sys.argv) > 5 and sys.argv[5] in ('2', '3'):
            for test in run('general_vary.dat'):
                m, n, qos_dims, user_reqs, user_class_demand, qos, tp = test
                print(m, n, sum(len(tp[c]) for c in range(n)), -1)
                mip.solve(deepcopy(test))
                proposed.solve(deepcopy(test), False)
                proposed.solve(deepcopy(test), True)
                mip_greedy.solve(deepcopy(test))
                print()
            print('---')

        sys.stdout.flush()

    print(np.median(tmp))
