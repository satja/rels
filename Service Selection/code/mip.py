
from copy import deepcopy
from pulp import *
from numpy.linalg import norm
from sklearn.cluster import KMeans
import numpy as np
import sys
from util import *

@timeit
def solve(test):
    m, n, qos_dims, user_reqs, user_class_demand, qos, tp = test
    services = [[i for i, x in enumerate(tp[c])] for c in range(n)]

    prob = LpProblem('clus_mip', LpMinimize)
    max_index = max(len(x) for x in services)
    x = LpVariable.dicts('choice',
            (list(range(m)), list(range(n)), list(range(max_index))),
            0, 1, LpInteger)
    for u in range(m):
        for c in range(n):
            prob += lpSum(x[u][c][i] for i in services[c]) == user_class_demand[u][c], ""
    for c in range(n):
        for i in services[c]:
            prob += lpSum(x[u][c][i] for u in range(m)) <= tp[c][i], ""
    for u in range(m):
        for d in range(qos_dims):
            prob += (float(user_reqs[u][d]) >= lpSum(
                lpSum(qos[c][d][u, i] * x[u][c][i] for i in services[c])
                for c in range(n)
                ))
    prob.solve()
    print(LpStatus[prob.status], file=sys.stderr)

    # Check the solution.
    satisfied = 0
    deviations = [[] for d in range(qos_dims)]
    for d in range(qos_dims):
        for u in range(m):
            q = 0
            for c in range(n):
                chosen = 0
                load = np.zeros(len(services[c]))
                for i in services[c]:
                    if x[u][c][i].value():
                        load[i] += x[u][c][i].value()
                        q += x[u][c][i].value() * qos[c][d][u, i]
                        chosen += x[u][c][i].value()
                assert np.all(load <= tp[c])
                assert abs(chosen - user_class_demand[u][c]) < 1e-5,\
                        (u, c, d, chosen, user_class_demand[u][c])
            satisfied += (q <= user_reqs[u][d])
            if user_reqs[u][d]:
                deviations[d].append((user_reqs[u][d] - q) / abs(user_reqs[u][d]))
    #print('MIP', sum(len(x) for x in services),
    #        LpStatus[prob.status], satisfied / (m * qos_dims))
    if LpStatus[prob.status] == 'Undefined':
        return None
    return satisfied / (m * qos_dims), [np.mean(x) for x in deviations]
