from copy import deepcopy
from transport3 import transport
from random import randrange
from bisect import bisect_left, bisect_right
from multiprocessing import cpu_count, Pool
import numpy as np
from util import *
from time import time
from math import exp, floor, ceil
from concurrent.futures import ProcessPoolExecutor
import sys

INFEASIBLE = 10**9

@timeit
def solve(test):
    m, n, qos_dims, user_reqs, user_class_demand, qos, tp = test
    services = [[i for i, _ in enumerate(tp[c])] for c in range(n)]

    satisfied = 0
    deviations = [[] for d in range(qos_dims)]
    x = np.zeros((m, n, max(len(s) for s in services)), dtype=int)
    
    for c in range(n):
        load = np.array(tp[c])
        for u in range(m):
            demand = user_class_demand[u][c]
            if demand == 0:
                continue
            utility = np.zeros(len(services[c]))
            for d in range(qos_dims):
                best_for_u = sorted(services[c], key=lambda i: qos[c][d][u, i])
                for pos, i in enumerate(best_for_u):
                    utility[i] += pos
            best_for_u = sorted(services[c], key=lambda i: utility[i])
            for i in best_for_u:
                if demand == 0:
                    break
                if load[i] == 0:
                    continue
                invocations = min(demand, load[i])
                demand -= invocations
                load[i] -= invocations
                x[u][c][i] = invocations

    for d in range(qos_dims):
        for u in range(m):
            q = 0
            for c in range(n):
                chosen = 0
                for i in services[c]:
                    q += x[u][c][i] * qos[c][d][u, i]
                    chosen += x[u][c][i]
                assert abs(chosen - user_class_demand[u][c]) < 1e-5,\
                        (u, c, d, chosen, user_class_demand[u][c])
            satisfied += (q <= user_reqs[u][d])
            if user_reqs[u][d]:
                deviations[d].append((user_reqs[u][d] - q) / abs(user_reqs[u][d]))

    return satisfied / (m * qos_dims), [np.mean(y) for y in deviations]
