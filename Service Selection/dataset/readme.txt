Binary files:

eq_qos.dat
general_vary.dat
single.dat
single_vary.dat
test.dat

contain Python objects saved in pickle format.

They correspond to the test scenarios generated and used by "test.py".
