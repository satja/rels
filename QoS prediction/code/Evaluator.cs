﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Diagnostics;

namespace TS2Eval
{

    class Evaluator
    {
        static int U_RANK;
        static int S_RANK;
        static int RANK = 7;

        DataLoader dl;

        public int status = 0;

        int currentDensity;
        ArrayList lucsDensity = new ArrayList();
        ArrayList cfDensity = new ArrayList();
        List<Tuple<int, int> > testingSet = new List<Tuple<int, int> >();
        ArrayList missingParam = new ArrayList();

        double[,] statisticsReal;
        double[,] statisticsCf;
        int[,] numSamples;
        bool[,] maskCf;

        public Evaluator(int numUsers, int numServices)
        {
            U_RANK = numUsers;
            S_RANK = numServices;
            statisticsReal = new double[U_RANK, S_RANK];
        }

        // tip 0 --> svugdje 150 uzoraka
        // tip 1 --> random 1-100
        private string[] opis_tipa = new string[2] { "numSamples: 150", "numSamples: random 1-100" };

        private void prepareSamples(int stohastic = 0)
        {
            numSamples = new int[U_RANK, S_RANK];
            Random rand = new Random(1234);
            for (int user = 0; user < U_RANK; ++user)
                for (int service = 0; service < S_RANK; ++service)
                    if (maskCf[user, service])
                    {
                        if (stohastic == 0)
                        {
                            numSamples[user, service] = 150;
                            continue;
                        }
                        numSamples[user, service] = 1 + rand.Next(100);

                        double num_success = 0;
                        for (int sample = 0; sample < numSamples[user, service]; ++sample)
                            num_success += (rand.NextDouble() < statisticsReal[user, service] ? 1 : 0);

                        statisticsCf[user, service] = num_success / numSamples[user, service];
                    }
                    else
                        numSamples[user, service] = 0;
        }

        public void evaluateNewChinese(string infile)
        {
            TextWriter file = new StreamWriter(System.Environment.CurrentDirectory +
                   "\\output\\" + infile.Substring(0, 15) + "_" + DateTime.Now.ToString("dd-MM_HH-mm") + ".txt", true);

            dl = new DataLoader(U_RANK, S_RANK);
            this.statisticsReal = dl.loadData(infile);
            createStatisticsCf();

            double[] rmspe = new double[4]{0, 0, 0, 0};
            double[] mape = new double[4] { 0, 0, 0, 0 };
            Stopwatch sw = new Stopwatch();

            for (int d = 5; d < 51; d += 5)
            {
                Console.WriteLine("loading density = {0}...", d);
                file.WriteLine("density = {0}", d);
                file.WriteLine();

                //loadDensity(d, "new_chinese_density");
                clearDensity();
                dl = new DataLoader(U_RANK, S_RANK);
                cfDensity.AddRange(dl.loadDensityLines(d, "new_chinese_density"));
                currentDensity = d;
                updateMaskFields();

                prepareSamples();
                generateAll(25000);
                
                Console.WriteLine("Initializing Hybrid...");
                sw.Restart();
                HybridPrediction hybrid = new HybridPrediction(statisticsCf, maskCf, statisticsReal,
                    U_RANK, S_RANK, 0, 1000, 15, 70, 0.9);
                sw.Stop();
                Console.WriteLine("Precompute time = {0}", sw.ElapsedMilliseconds / 1000.0);
                file.WriteLine("Precompute time = {0}", sw.ElapsedMilliseconds / 1000.0);

                Console.WriteLine("Running Hybrid...");
                sw.Restart();
                Tuple<double, double> rmspeANDmapeH = hybrid.calculateRMSPEandMAPE(testingSet);
                sw.Stop();
                Console.WriteLine("Prediction time = {0}", sw.ElapsedMilliseconds / 1000.0);
                Console.WriteLine("Hybrid:\t\t{0},\t\t{1}", rmspeANDmapeH.Item1, rmspeANDmapeH.Item2);
                file.WriteLine("Prediction time = {0}", sw.ElapsedMilliseconds / 1000.0);
                file.WriteLine("Hybrid:\t\t{0},\t\t{1}", rmspeANDmapeH.Item1, rmspeANDmapeH.Item2);
                file.WriteLine();

                rmspe[0] += rmspeANDmapeH.Item1;
                mape[0] += rmspeANDmapeH.Item2;

                hybrid = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                
                for (int i = 1; i <= 3; ++i)
                {
                    Matrix.S = 10;
                    if (i == 2) Matrix.S = 100;
                    if (i == 3) Matrix.S = 1000;
                    file.WriteLine("S = {0}", Matrix.S);
                    Console.WriteLine("S = {0}", Matrix.S);

                    Console.WriteLine("Initializing SPBP...");
                    sw.Restart();
                    SimilarPairsBasedPrediction spbp = new SimilarPairsBasedPrediction(
                        statisticsCf, maskCf, numSamples, statisticsReal, U_RANK, S_RANK, 0, 1000, 15, 70, 0.9);
                    sw.Stop();
                    Console.WriteLine("Precompute time = {0}", sw.ElapsedMilliseconds / 1000.0);
                    file.WriteLine("Precompute time = {0}", sw.ElapsedMilliseconds / 1000.0);

                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    Console.WriteLine("Running SPBP...");
                    sw.Restart();
                    Tuple<double, double> rmspeANDmape = spbp.calculateRMSPEandMAPE(testingSet);
                    sw.Stop();
                    Console.WriteLine("Prediction time = {0}", sw.ElapsedMilliseconds / 1000.0);
                    file.WriteLine("Prediction time = {0}", sw.ElapsedMilliseconds / 1000.0);
                    file.WriteLine("SPBP:  \t\t{0},\t\t{1}", rmspeANDmape.Item1, rmspeANDmape.Item2);
                    Console.WriteLine("SPBP:  \t\t{0},\t\t{1}", rmspeANDmape.Item1, rmspeANDmape.Item2);
                    file.WriteLine();

                    spbp = null;
                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    rmspe[i] += rmspeANDmape.Item1;
                    mape[i] += rmspeANDmape.Item2;
                }

                file.WriteLine();
            }
            file.WriteLine("Hybrid:          avg RMSPE = {0}, avg MAPE = {1}", rmspe[0] / 10, mape[0] / 10);
            file.WriteLine("SPBP (S = 10):   avg RMSPE = {0}, avg MAPE = {1}", rmspe[1] / 10, mape[1] / 10);
            file.WriteLine("SPBP (S = 100):  avg RMSPE = {0}, avg MAPE = {1}", rmspe[2] / 10, mape[2] / 10);
            file.WriteLine("SPBP (S = 1000): avg RMSPE = {0}, avg MAPE = {1}", rmspe[3] / 10, mape[3] / 10);
            file.Close();
        }

        public void evaluateChinese(string infile)
        {
            Matrix.S = 300;

            TextWriter file = new StreamWriter(System.Environment.CurrentDirectory +
                   "\\output\\" + infile.Substring(0, 11) + "_" + DateTime.Now.ToString("dd-MM_HH-mm") + ".txt", true);

            file.WriteLine("S = {0}", Matrix.S);

            dl = new DataLoader(U_RANK, S_RANK);
            this.statisticsReal = dl.loadData(infile);
            createStatisticsCf();

            double s = 0, sh = 0, ss = 0, ssh = 0;
            Stopwatch sw = new Stopwatch();

            for (int d = 5; d < 51; d += 5)
            {
                Console.WriteLine("loading density = {0}...", d);
                file.WriteLine("density = {0}", d);

                //loadDensity(d, "densityChinese");
                clearDensity();
                dl = new DataLoader(U_RANK, S_RANK);
                cfDensity.AddRange(dl.loadDensityLines(d, "chinese_density"));
                currentDensity = d;
                updateMaskFields();

                prepareSamples();
                generateAll();
                
                Console.WriteLine("Initializing Hybrid...");
                sw.Restart();
                HybridPrediction hybrid = new HybridPrediction(statisticsCf, maskCf, statisticsReal,
                    U_RANK, S_RANK, 0, 1000, 15, 70, infile == "kineski2_RTmatrix.txt" ? 0.4 : 0.9);
                sw.Stop();
                Console.WriteLine("Precompute time = {0}", sw.ElapsedMilliseconds / 1000.0);
                file.WriteLine("Precompute time = {0}", sw.ElapsedMilliseconds / 1000.0);
                Console.WriteLine("Running Hybrid...");
                sw.Restart();
                Tuple<double, double> rmspeANDmapeH = hybrid.calculateRMSPEandMAPE(testingSet);
                sw.Stop();
                Console.WriteLine("Prediction time = {0}", sw.ElapsedMilliseconds / 1000.0);
                file.WriteLine("Prediction time = {0}", sw.ElapsedMilliseconds / 1000.0);
                file.WriteLine("Hybrid:\t\t{0},\t\t{1}", rmspeANDmapeH.Item1, rmspeANDmapeH.Item2);
                Console.WriteLine("Hybrid:\t\t{0},\t\t{1}", rmspeANDmapeH.Item1, rmspeANDmapeH.Item2);

            
                Console.WriteLine("Initializing SPBP...");
                sw.Restart();
                SimilarPairsBasedPrediction spbp = new SimilarPairsBasedPrediction(
                    statisticsCf, maskCf, numSamples, statisticsReal, U_RANK, S_RANK, 0, 1000, 15, 70, 0.5);
                sw.Stop();
                Console.WriteLine("Precompute time = {0}", sw.ElapsedMilliseconds / 1000.0);
                file.WriteLine("Precompute time = {0}", sw.ElapsedMilliseconds / 1000.0);

                System.Threading.Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                System.Threading.Thread.Sleep(5000);

                Console.WriteLine("Running SPBP...");
                sw.Restart();
                Tuple<double, double> rmspeANDmape = spbp.calculateRMSPEandMAPE(testingSet);
                sw.Stop();
                Console.WriteLine("Prediction time = {0}", sw.ElapsedMilliseconds / 1000.0);
                file.WriteLine("Prediction time = {0}", sw.ElapsedMilliseconds / 1000.0);
                file.WriteLine("SPBP:  \t\t{0},\t\t{1}", rmspeANDmape.Item1, rmspeANDmape.Item2);
                Console.WriteLine("SPBP:  \t\t{0},\t\t{1}", rmspeANDmape.Item1, rmspeANDmape.Item2);
                file.WriteLine();
                file.WriteLine();
                
                spbp = null;
                System.Threading.Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                System.Threading.Thread.Sleep(5000);
                 

                s += rmspeANDmape.Item1;
                //sh += rmspeANDmapeH.Item1;
                ss += rmspeANDmape.Item2;
                //ssh += rmspeANDmapeH.Item2;
            }
            file.WriteLine("Hybrid: avg RMSPE = {0}, avg MAPE = {1}", sh / 10, ssh / 10);
            file.WriteLine("SPBP:   avg RMSPE = {0}, avg MAPE = {1}", s / 10, ss / 10);
            file.Close();
        }

        public void evaluateDensityImpact(string infile, string densFile, int stohastic = 0)
        {
            string outfile = (stohastic > 0 ? "random_sample_" : "full_sample_");
            if (infile == "chineseFP.txt")
            {
                outfile = "FP_sample_" + (stohastic > 0 ? "random_" : "");
            }
            TextWriter file = new StreamWriter(System.Environment.CurrentDirectory +
                    "\\output\\" + outfile + DateTime.Now.ToString("dd-MM_HH-mm") + ".txt", true);

            dl = new DataLoader(U_RANK, S_RANK);
            this.statisticsReal = dl.loadData(infile);
            createStatisticsCf();

            double s = 0, sh = 0, ss = 0, ssh = 0;

            for (int d = 5; d < 51; d += 5)
            {
                Console.WriteLine("loading density = {0}...", d);
                file.WriteLine("density = {0}", d);

                loadDensity(d, densFile);
                prepareSamples(stohastic);
                generateAll();

                Tuple<double, int, int> hybridParams;
                Tuple<double, int, int> spbpParams;

                if (stohastic == 1)
                {
                    hybridParams = spbpParams = new Tuple<double, int, int>(0.2, 25, 25);
                }
                else if (infile != "chineseFP.txt")
                {
                    hybridParams = new Tuple<double, int, int>(0.1, 4, 12);
                    spbpParams = new Tuple<double, int, int>(0.1, 4, 5);
                }
                else
                {
                    hybridParams = new Tuple<double, int, int>(0.3, 14, 15);
                    spbpParams = new Tuple<double, int, int>(0.8, 29, 1);
                }

                Console.WriteLine("Initializing Hybrid...");
                HybridPrediction hybrid = new HybridPrediction(statisticsCf, maskCf, statisticsReal,
                    U_RANK, S_RANK, 0, 1, hybridParams.Item2, hybridParams.Item3, hybridParams.Item1);
                Console.WriteLine("Running Hybrid...");
                Tuple<double, double> rmseANDmaeH = hybrid.calculateRMSEandMAE(testingSet, file);
                file.WriteLine("Hybrid:\t\t{0},\t\t{1}", rmseANDmaeH.Item1, rmseANDmaeH.Item2);
                Console.WriteLine("Hybrid:\t\t{0},\t\t{1}", rmseANDmaeH.Item1, rmseANDmaeH.Item2);

                Console.WriteLine("Initializing SPBP...");
                SimilarPairsBasedPrediction spbp = new SimilarPairsBasedPrediction(
                    statisticsCf, maskCf, numSamples, statisticsReal, U_RANK, S_RANK, 0, 1,
                    spbpParams.Item2, spbpParams.Item3, spbpParams.Item1, stohastic);
                Console.WriteLine("Running SPBP...");
                Tuple<double, double> rmseANDmae = spbp.calculateRMSEandMAE(testingSet, file);
                file.WriteLine("SPBP:  \t\t{0},\t\t{1}", rmseANDmae.Item1, rmseANDmae.Item2);
                Console.WriteLine("SPBP:  \t\t{0},\t\t{1}", rmseANDmae.Item1, rmseANDmae.Item2);
                file.WriteLine();
                file.WriteLine();

                s += rmseANDmae.Item1;
                sh += rmseANDmaeH.Item1;
                ss += rmseANDmae.Item2;
                ssh += rmseANDmaeH.Item2;
            }
            file.WriteLine("Hybrid: avg RMSE = {0}, avg MAE = {1}", sh / 10, ssh / 10);
            file.WriteLine("SPBP:   avg RMSE = {0}, avg MAE = {1}", s / 10, ss / 10);
            file.Close();
            Console.WriteLine("Hybrid: avg RMSE = {0}, avg MAE = {1}", sh / 10, ssh / 10);
            Console.WriteLine("SPBP:   avg RMSE = {0}, avg MAE = {1}", s / 10, ss / 10);
        }

        private struct Dogadjaj
        {
            public int user;
            public int service;
            public int tip;
            public Dogadjaj(int u, int s, int t) { user = u; service = s; tip = t; }

            // Tipovi dogadjaja.
            public static int FAIL = 0;
            public static int SUCCESS = 1;
            public static int PREDICT = 2;
        }

        // Simulacija stohastickoga procesa.
        public void evaluateStohastic(string infile, string densFile, int predictions = 400, int updates = 80000)
        {
            dl = new DataLoader(U_RANK, S_RANK);
            this.statisticsReal = dl.loadData(infile);

            TextWriter file = new StreamWriter(System.Environment.CurrentDirectory +
                "\\output\\stohastic_output_" + (infile == "chineseFP.txt" ? "FP_" : "")
                + DateTime.Now.ToString("dd-MM_HH-mm") + ".txt", true);

            double maeStatic = 0;
            double maeRealtime = 0;
            double rmseStatic = 0;
            double rmseRealtime = 0;
            double timeStatic = 0;
            double timeRealtime = 0;

            Stopwatch swStatic = new Stopwatch();
            Stopwatch swRealtime = new Stopwatch();

            for (int d = 5; d < 51; d += 5)
            {
                createStatisticsCf();
                loadDensity(d, densFile);
                prepareSamples(1);
                Console.WriteLine("density = {0}", d);

                Random rand = new Random();
                List<Dogadjaj> events = new List<Dogadjaj>();

                // Generate prediction events.
                for (int i = 0; i < predictions; ++i)
                {
                    int user = rand.Next(0, U_RANK);
                    int service = rand.Next(0, S_RANK);
                    if (statisticsReal[user, service] != -1)
                        events.Add(new Dogadjaj(user, service, Dogadjaj.PREDICT));
                    else --i;
                }

                // Generate update events.
                for (int i = 0; i < updates; ++i)
                {
                    int user = rand.Next(0, U_RANK);
                    int service = rand.Next(0, S_RANK);
                    if (statisticsReal[user, service] == -1)
                    {
                        --i;
                        continue;
                    }
                    int zapis = (rand.NextDouble() < statisticsReal[user, service] ?
                        Dogadjaj.SUCCESS : Dogadjaj.FAIL);
                    events.Add(new Dogadjaj(user, service, zapis));
                }

                // Shuffle events.
                events = events.OrderBy(x => rand.Next()).ToList();

                SimilarPairsBasedPrediction spbpRealtime = new SimilarPairsBasedPrediction(
                    statisticsCf, maskCf, numSamples, statisticsReal, U_RANK, S_RANK, 0, 1, U_RANK, S_RANK, -1, 1);

                SimilarPairsBasedPrediction spbpStatic = new SimilarPairsBasedPrediction(
                    statisticsCf, maskCf, numSamples, statisticsReal, U_RANK, S_RANK, 0, 1, 25, 25, 0.2, 1);

                swStatic.Restart();
                Tuple<double, double> rmseANDmaeStatic = runEvents(spbpStatic, events, false);
                swStatic.Stop();
                timeStatic += swStatic.ElapsedMilliseconds / 1000.0;

                swRealtime.Restart(); 
                Tuple<double, double> rmseANDmaeRealtime = runEvents(spbpRealtime, events);
                swRealtime.Stop();
                timeRealtime += swStatic.ElapsedMilliseconds / 1000.0;

                rmseRealtime += rmseANDmaeRealtime.Item1;
                maeRealtime += rmseANDmaeRealtime.Item2;

                rmseStatic += rmseANDmaeStatic.Item1;
                maeStatic += rmseANDmaeStatic.Item2;

                file.WriteLine("density = {0}", d);
                file.WriteLine("RMSE (realtime): {0}, \t MAE (realtime): {1}, \t time (realtime) : {2}",
                    rmseANDmaeRealtime.Item1, rmseANDmaeRealtime.Item2, swRealtime.ElapsedMilliseconds / 1000.0);
                file.WriteLine("RMSE (static):   {0}, \t MAE (static):   {1}, \t time (static) : {2}",
                    rmseANDmaeStatic.Item1, rmseANDmaeStatic.Item2, swStatic.ElapsedMilliseconds / 1000.0);
                file.WriteLine();
                file.WriteLine();
            }
            file.WriteLine("Realtime: avg RMSE = {0}, \t avg MAE = {1}, \t avg time = {2}",
                rmseRealtime / 10, maeRealtime / 10, timeRealtime / 10);
            file.WriteLine("Static  : avg RMSE = {0}, \t avg MAE = {1}, \t avg time = {2}",
                rmseStatic / 10, maeStatic / 10, timeStatic / 10);
            file.Close();

            Console.WriteLine("Realtime: avg RMSE = {0}, \t avg MAE = {1}", rmseRealtime / 10, maeRealtime / 10);
            Console.WriteLine("Static  : avg RMSE = {0}, \t avg MAE = {1}", rmseStatic / 10, maeStatic / 10);
        }

        private Tuple<double, double> runEvents(
            SimilarPairsBasedPrediction spbp, List<Dogadjaj> events, bool realtime = true, TextWriter file = null)
        {
            Stopwatch s_update = new Stopwatch();
            Stopwatch s_predict = new Stopwatch();

            int count = 0;
            double mae = 0;
            double rmse = 0;
            for (int i = 0; i < events.Count; ++i)
            {
                if (events[i].tip != Dogadjaj.PREDICT)
                {
                    if (realtime)
                    {
                        s_update.Start();
                        spbp.update(events[i].user, events[i].service, events[i].tip);
                        s_update.Stop();
                    }
                    continue;
                }

                s_predict.Start();
                double cf = spbp.predictionInPoint(events[i].user, events[i].service);
                s_predict.Stop();
                mae += Math.Abs(cf - statisticsReal[events[i].user, events[i].service]);
                rmse += Math.Pow(cf - statisticsReal[events[i].user, events[i].service], 2);
                ++count;
            }
            if (file != null)
                file.WriteLine("avg update time: {0}, \t avg predict time = {1}, \t total update time = {2}, \t total predict time = {3}",
                    s_update.ElapsedMilliseconds / 1000.0 / (events.Count - count),
                    s_predict.ElapsedMilliseconds / 1000.0 / count,
                    s_update.ElapsedMilliseconds / 1000.0,
                     s_predict.ElapsedMilliseconds / 1000.0);
            return new Tuple<double, double>(Math.Sqrt(rmse / count), mae / count);
        }

        private void loadDensity(int d, string filename)
        {
            clearDensity();
            dl = new DataLoader(U_RANK, S_RANK);
            cfDensity.AddRange(dl.loadDensityCf(d, filename));
            currentDensity = d;
            updateMaskFields();
        }

        private void clearDensity()
        {
            lucsDensity.Clear();
            cfDensity.Clear();
            updateMaskFields();
        }

        private void updateMaskFields()
        {
            maskCf = new bool[U_RANK, S_RANK];
            foreach (int x in cfDensity)
                if (statisticsReal[x / S_RANK, x % S_RANK] != -1)
                    maskCf[x / S_RANK, x % S_RANK] = true;
        }

        private void createStatisticsCf()
        {
            statisticsCf = new double[U_RANK, S_RANK];
            for (int u = 0; u < U_RANK; u++)
                for (int s = 0; s < S_RANK; s++)
                    statisticsCf[u, s] = statisticsReal[u, s];
        }

        // Generate testing set for over the whole space.
        private void generateAll(int partial = 0)
        {
            testingSet.Clear();
            for (int u = 0; u < U_RANK; u++)
                for (int s = 0; s < S_RANK; ++s)
                    if (!maskCf[u, s] && statisticsReal[u, s] != -1)
                        testingSet.Add(new Tuple<int, int>(u, s));

            if (partial == 0) return;
            Random rand = new Random(345);
            testingSet = testingSet.OrderBy(i => rand.Next()).Take(partial).ToList();
        }

        private void createStatisticsReal(double[, ,] sve, int l)
        {
            for (int u = 0; u < U_RANK; ++u)
                for (int s = 0; s < S_RANK; ++s)
                    statisticsReal[u, s] = sve[l, u, s];
        }

        // Simulacija promjenjivog stohastickog procesa.
        public void evaluateChangingStohastic(string infile, int predictions = 1000, int updates = 200000)
        {
            dl = new DataLoader(U_RANK, S_RANK);
            double[, ,] sve = dl.loadDataDifferentQoS(infile);

            TextWriter file = new StreamWriter(System.Environment.CurrentDirectory +
                "\\output\\changing_stohastic_output_" + DateTime.Now.ToString("dd-MM_HH-mm") + ".txt", true);

            createStatisticsReal(sve, 0);
            //createStatisticsCf();

            SimilarPairsBasedPrediction spbp = new SimilarPairsBasedPrediction(
                new double[U_RANK, S_RANK], new bool[U_RANK, S_RANK], new int[U_RANK, S_RANK],
                statisticsReal, U_RANK, S_RANK, 0, 1, U_RANK, S_RANK, -1, 2);

            SimilarPairsBasedPrediction control = new SimilarPairsBasedPrediction(
                new double[U_RANK, S_RANK], new bool[U_RANK, S_RANK], new int[U_RANK, S_RANK],
                statisticsReal, U_RANK, S_RANK, 0, 1, U_RANK, S_RANK, -1, 1);

            Stopwatch sw = new Stopwatch();

            for (int l = 0; l < RANK; ++l)
            {
                createStatisticsReal(sve, l);
                //createStatisticsCf();
                //loadDensity(d, "densityCf");
                //prepareSamples(true);

                Random rand = new Random();
                List<Dogadjaj> events = new List<Dogadjaj>();

                // Generate prediction events.
                for (int i = 0; i < predictions; ++i)
                {
                    int user = rand.Next(0, U_RANK);
                    int service = rand.Next(0, S_RANK);
                    if (statisticsReal[user, service] != -1)
                        events.Add(new Dogadjaj(user, service, Dogadjaj.PREDICT));
                    else --i;
                }

                // Generate update events.
                for (int i = 0; i < updates; ++i)
                {
                    int user = rand.Next(0, U_RANK);
                    int service = rand.Next(0, S_RANK);
                    if (statisticsReal[user, service] == -1)
                    {
                        --i;
                        continue;
                    }
                    int zapis = (rand.NextDouble() < statisticsReal[user, service] ?
                        Dogadjaj.SUCCESS : Dogadjaj.FAIL);
                    events.Add(new Dogadjaj(user, service, zapis));
                }

                // Shuffle events.
                events = events.OrderBy(x => rand.Next()).ToList();

                sw.Restart();
                Tuple<double, double> rmseANDmae = runEvents(spbp, events);
                sw.Stop();
                file.WriteLine("load = {2}, RMSE (pravi):   {0}, \t MAE (pravi):   {1}, \t time (pravi) = {3}",
                    rmseANDmae.Item1, rmseANDmae.Item2, l, sw.ElapsedMilliseconds / 1000.0);

                sw.Restart();
                Tuple<double, double> rmseANDmaeControl = runEvents(control, events);
                sw.Stop();

                file.WriteLine("load = {2}, RMSE (control): {0}, \t MAE (control): {1}, \t time (control) = {3}",
                    rmseANDmaeControl.Item1, rmseANDmaeControl.Item2, l, sw.ElapsedMilliseconds / 1000.0);

                file.WriteLine();
                Console.WriteLine("Finished load = {0}", l);
            }
            file.Close();
        }


        // Simulacija povecanja broja predikcija i updateova.
        public void evaluateScalability(string infile, int predictions = 10, int updates = 10)
        {
            string outfile = "scalability_";
            if (infile == "chineseFP.txt")
            {
                outfile += "FP_";
            }
            TextWriter file = new StreamWriter(System.Environment.CurrentDirectory +
                    "\\output\\" + outfile + DateTime.Now.ToString("dd-MM_HH-mm") + ".txt", true);

            dl = new DataLoader(U_RANK, S_RANK);
            this.statisticsReal = dl.loadData(infile);

            //double[, ,] sve = dl.loadDataDifferentQoS(infile);

            //createStatisticsReal(sve, 0);
            //createStatisticsCf();

            SimilarPairsBasedPrediction spbp = new SimilarPairsBasedPrediction(
                new double[U_RANK, S_RANK], new bool[U_RANK, S_RANK], new int[U_RANK, S_RANK],
                statisticsReal, U_RANK, S_RANK, 0, 1, U_RANK, S_RANK, -1, 1);

            SimilarPairsBasedPrediction control = new SimilarPairsBasedPrediction(
                new double[U_RANK, S_RANK], new bool[U_RANK, S_RANK], new int[U_RANK, S_RANK],
                statisticsReal, U_RANK, S_RANK, 0, 1, U_RANK, S_RANK, -1, 1);


            Stopwatch sw = new Stopwatch();

            for (int l = 0; l < 5; ++l)
            {
                predictions *= 10;
                updates *= 10;

                //this.statisticsReal = dl.loadData(infile);
                //createStatisticsCf();
                //loadDensity(d, "densityCf");
                //prepareSamples(true);

                Random rand = new Random();
                List<Dogadjaj> events = new List<Dogadjaj>();

                // Generate prediction events.
                for (int i = 0; i < predictions; ++i)
                {
                    int user = rand.Next(0, U_RANK);
                    int service = rand.Next(0, S_RANK);
                    if (statisticsReal[user, service] != -1)
                        events.Add(new Dogadjaj(user, service, Dogadjaj.PREDICT));
                    else --i;
                }

                // Generate update events.
                for (int i = 0; i < updates; ++i)
                {
                    int user = rand.Next(0, U_RANK);
                    int service = rand.Next(0, S_RANK);
                    if (statisticsReal[user, service] == -1)
                    {
                        --i;
                        continue;
                    }
                    int zapis = (rand.NextDouble() < statisticsReal[user, service] ?
                        Dogadjaj.SUCCESS : Dogadjaj.FAIL);
                    events.Add(new Dogadjaj(user, service, zapis));
                }

                // Shuffle events.
                events = events.OrderBy(x => rand.Next()).ToList();

                sw.Restart();
                Tuple<double, double> rmseANDmae = runEvents(spbp, events, true, file);
                sw.Stop();
                file.WriteLine("load = {2}, RMSE (pravi):   {0}, \t MAE (pravi):   {1}, \t time (pravi) = {3}",
                    rmseANDmae.Item1, rmseANDmae.Item2, l, sw.ElapsedMilliseconds / 1000.0);

                sw.Restart();
                Tuple<double, double> rmseANDmaeControl = runEvents(control, events, false, file);
                sw.Stop();

                file.WriteLine("load = {2}, RMSE (control): {0}, \t MAE (control): {1}, \t time (control) = {3}",
                    rmseANDmaeControl.Item1, rmseANDmaeControl.Item2, l, sw.ElapsedMilliseconds / 1000.0);

                file.WriteLine();
                Console.WriteLine("Finished load = {0}", l);
            }
            file.Close();
        }
    }
}
