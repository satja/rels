﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;

namespace TS2Eval
{
    class DataLoader
    {
        static int U_RANK;
        static int S_RANK;
        static int RANK = 7;

        string currentDirectory = System.Environment.CurrentDirectory;

        public DataLoader(int numUsers, int numServices)
        {
            U_RANK = numUsers;
            S_RANK = numServices;
        }

        public double[,] loadData(string filename)
        {
            double[,] statisticsReal = new double[U_RANK, S_RANK];
            string[] lines = File.ReadAllLines(currentDirectory + "\\input\\" + filename);
            int currentLine = 0;
            for (int u = 0; u < U_RANK; u++)
            {
                string [] services = lines[currentLine].Split('\t');
                for (int s = 0; s < S_RANK; s++)
                    statisticsReal[u, s] = Convert.ToDouble(services[s], CultureInfo.InvariantCulture);
                currentLine++;
            }
            return statisticsReal;
        }

        public int[] loadDensityCf(int density, string filename)
        {
            string path = currentDirectory + "\\densities\\";
            string[] d = File.ReadAllLines(path + filename + "_" + density.ToString() + ".txt");
            int[] dn = new int[d.Length];
            for (int i = 0; i < d.Length; i++)
                dn[i] = Convert.ToInt32(d[i]);
            return dn;
        }

        public int[] loadDensityLines(int density, string filename)
        {
            string path = currentDirectory + "\\densities\\";
            int lines = U_RANK * S_RANK * density / 100;
            string[] d = File.ReadLines(path + filename + ".txt").Take(lines).ToArray();
            int[] dn = new int[d.Length];
            for (int i = 0; i < d.Length; i++)
                dn[i] = Convert.ToInt32(d[i]);
            return dn;
        }

        public double[, ,] loadDataDifferentQoS(string filename)
        {
            double[, ,] statisticsReal = new double[RANK, U_RANK, S_RANK];
            string[] lines = File.ReadAllLines(currentDirectory + "\\input\\" + filename);
            int currentLine = 0;
            for (int l = 0; l < RANK; l++)
            {
                for (int u = 0; u < U_RANK; u++)
                {
                    string[] services = lines[currentLine].Split('\t');
                    for (int s = 0; s < S_RANK; s++)
                            statisticsReal[l, u, s] =
                                Convert.ToDouble(services[s], CultureInfo.InvariantCulture);
                    currentLine++;
                }
                currentLine++;
            }
            return statisticsReal;
        }
    }
}
