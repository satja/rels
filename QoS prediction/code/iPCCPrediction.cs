﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TS2Eval
{
    class iPCCPrediction:CFPrediction
    {
        int sumBestK = 0;
        int numPredictions = 0;

        public iPCCPrediction(double[,] source, bool[,] mask, double[,] real,
            int numUsers, int numServices, int minValue, int maxValue)
            : base(source, mask, real, numUsers, numServices, minValue, maxValue)
        {
        }

        public double bestCount() { return (double)sumBestK / numPredictions; }

        // uPCC in point
        public override double predictionInPoint(int user, int currentService)
        {
            double currentAverage = serviceAvg[currentService];
            double impact = 0;
            double aggregated = 0;
            int count = 0;
            double bestPrediction = 1000;
            int bestCount = -1;
            foreach (int service in similarServices[currentService])
                if (mask[user, service])
                {
                    aggregated += serviceSim[currentService, service];
                    impact += serviceSim[currentService, service] * (source[user, service] - serviceAvg[service]);
                    
                    ++count;

                    if (Math.Abs(currentAverage + impact / aggregated - real[user, currentService])
                        < Math.Abs(bestPrediction - real[user, currentService]))
                    {
                        bestPrediction = currentAverage + impact / aggregated;
                        bestCount = count;
                    }
                }
            if (aggregated == 0) return currentAverage;

            sumBestK += bestCount;
            ++numPredictions;

            return bestPrediction;
        }
    }
}
