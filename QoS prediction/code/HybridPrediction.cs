﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace TS2Eval
{
    class HybridPrediction : CFPrediction
    {
        public HybridPrediction(double[,] source, bool[,] mask, double[,] real,
            int numUsers, int numServices, double minValue, double maxValue,
            int topkUsers, int topkServices, double lambda)
            : base(source, mask, real, numUsers, numServices, minValue, maxValue, topkUsers, topkServices, lambda)
        {
            precomputeSimilarities();
            //precomputeSimilar();
        }

        protected override double calculateServicesImpact(int user, int currentService)
        {
            double impact = 0;
            double aggregated = 0;
            int count = 0;
            foreach (int service in mostSimilar(currentService, serviceSim, S_RANK)) // similarServices[currentService])
                if (mask[user, service])
                {
                    aggregated += serviceSim[currentService, service];
                    impact += serviceSim[currentService, service] * (source[user, service] - serviceAvg[service]);
                    if (++count == topkServices) break;
                }
            return (aggregated != 0) ? (serviceAvg[currentService] + impact / aggregated) : serviceAvg[currentService];
        }

        protected override double calculateUsersImpact(int currentUser, int service)
        {
            double impact = 0;
            double aggregated = 0;
            int count = 0;
            foreach (int user in mostSimilar(currentUser, userSim, U_RANK)) // similarUsers[currentUser])
                if (mask[user, service])
                {
                    aggregated += userSim[currentUser, user];
                    impact += userSim[currentUser, user] * (source[user, service] - userAvg[user]);
                    if (++count == topkUsers) break;
                }
            return (aggregated != 0) ? (userAvg[currentUser] + impact / aggregated) : userAvg[currentUser];
        }

        public override double predictionInPoint(int user, int service)
        {
            if (mask[user, service]) return source[user, service];
            double ret = lambdaPrediction(
                calculateUsersImpact(user, service), calculateServicesImpact(user, service));
            return Math.Max(Math.Min(ret, maxValue), minValue);
        }
    }
}
