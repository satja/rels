﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace TS2Eval
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private const int DEFAULT_TS_WIN_SIZE = 1;

        private void button2_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(50, 49);
            ev.evaluateDensityImpact("differentQoS.txt", "densityCf");
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(50, 49);
            ev.evaluateStohastic("differentQoS.txt", "densityCf");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(50, 49);
            ev.evaluateDensityImpact("differentQoS.txt",  "densityCf", 1);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(339, 5825);
            ev.evaluateChinese("kineski2_RTmatrix.txt");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(339, 5825);
            ev.evaluateChinese("kineski2_TPmatrix.txt");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(50, 49);
            //ev.UPCCandIPCC("differentQoS.txt", "densityCf");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(339, 5825);
            //ev.UPCCandIPCC("kineski2_RTmatrix.txt", "densityChinese");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(339, 5825);
            //ev.UPCCandIPCC("kineski2_TPmatrix.txt", "densityChinese");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(339, 5825);
            //ev.UPCCandIPCC("kineski2_RTmatrix.txt", "densityChinese");
            ev = new Evaluator(339, 5825);
            //ev.UPCCandIPCC("kineski2_TPmatrix.txt", "densityChinese");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(150, 100);
            ev.evaluateDensityImpact("chineseFP.txt", "chineseFPdensity");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(50, 49);
            ev.evaluateChangingStohastic("differentQoS.txt");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(150, 100);
            ev.evaluateDensityImpact("chineseFP.txt", "chineseFPdensity", 1);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(150, 100);
            ev.evaluateStohastic("chineseFP.txt", "chineseFPdensity", 800, 160000);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(5763, 5825);
            ev.evaluateNewChinese("new_kineski2_RTmatrix.txt");
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(5763, 5825);
            ev.evaluateNewChinese("new_kineski2_TPmatrix.txt");
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(150, 100);
            ev.evaluateScalability("chineseFP.txt");
        }

        private void button17_Click(object sender, EventArgs e)
        {
            Evaluator ev = new Evaluator(50, 49);
            ev.evaluateScalability("differentQoS.txt");
        }
        
    }
}
