﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace TS2Eval
{
    class  GenericPrediction
    {
        protected static int U_RANK;
        protected static int S_RANK;
       
        public GenericPrediction()
        {
        }

        public virtual double calculateRMSE(ArrayList testingSet) 
        { 
            return 0;
        }

        public virtual double calculateMAE(ArrayList testingSet)
        {
            return 0;
        }

        public virtual double predictionInPoint(int u, int s)
        {
            return 0;
        }

        public virtual double calculateMAEMissing(ArrayList testingSet, ArrayList missing, int missingCode)
        {
            return 0;
        }

        public virtual double calculateRMSEMissing(ArrayList testingSet, ArrayList missing, int missingCode)
        {
            return 0;
        }

        public virtual double calculateMAENoise(ArrayList testingSet, ArrayList noised, int noiseCode)
        {
            return 0;
        }

        public virtual double calculateRMSENoise(ArrayList testingSet, ArrayList noised, int noiseCode)
        {
            return 0;
        }

        public virtual double[,] calculatePredictions()
        {
            return new double[U_RANK, S_RANK];
            
        }

        public virtual int getClusterForService(int serviceId)
        {
            return -1;
        }

        // Helpful functions
        protected int[] sortSimilarity(double[] similarity, int dim)
        {
            int[] indexes = new int[dim];
            for (int i = 0; i < dim; i++) indexes[i] = i;
            for (int i = 0; i < similarity.Length; i++)
            {
                for (int j = i + 1; j < similarity.Length; j++)
                {

                    if (similarity[j] > similarity[i])
                    {
                        int reg1 = indexes[i];
                        indexes[i] = indexes[j];
                        indexes[j] = reg1;
                        double reg2 = similarity[i];
                        similarity[i] = similarity[j];
                        similarity[j] = reg2;

                    }
                }
            }
            return indexes;
        }

        /*
        // Extract the indexes from the random number.
        protected int[] convertToIndexes(int random)
        {
            int[] indexes = new int[4];
            int rest;
            indexes[0] = random / (RANK  * RANK * RANK);

            rest = random - indexes[0] * RANK * RANK * RANK;
            indexes[1] = rest / (RANK * RANK);
            rest = rest - indexes[1] * RANK * RANK;
            indexes[2] = rest / RANK;
            indexes[3] = rest - indexes[2] * RANK;
            return indexes;
        }
         */
    }
}
