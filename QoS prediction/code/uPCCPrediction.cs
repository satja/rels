﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TS2Eval
{
    class uPCCPrediction:CFPrediction
    {
        int sumBestK = 0;
        int numPredictions = 0;

        public uPCCPrediction(double[,] source, bool[,] mask, double[,] real,
            int numUsers, int numServices, int minValue, int maxValue)
            : base(source, mask, real, numUsers, numServices, minValue, maxValue)
        {
        }

        public double bestCount() { return (double)sumBestK / numPredictions; }

        // uPCC in point
        public override double predictionInPoint(int currentUser, int service)
        {
            double currentAverage = userAvg[currentUser];
            double impact = 0;
            double aggregated = 0;
            int count = 0;
            double bestPrediction = 1000;
            int bestCount = -1;
            foreach (int user in similarUsers[currentUser])
                if (mask[user, service])
                {
                    aggregated += userSim[currentUser, user];
                    impact += userSim[currentUser, user] * (source[user, service] - userAvg[user]);
                    
                    ++count;

                    if (Math.Abs(currentAverage + impact / aggregated - real[currentUser, service])
                        < Math.Abs(bestPrediction - real[currentUser, service]))
                    {
                        bestPrediction = currentAverage + impact / aggregated;
                        bestCount = count;
                    }
                }
            if (aggregated == 0) return currentAverage;

            sumBestK += bestCount;
            ++numPredictions;

            return bestPrediction;
        }
    }
}
