﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;

namespace TS2Eval
{
    abstract class CFPrediction:GenericPrediction
    {
        protected double lambda;
        protected int topkUsers;
        protected int topkServices;
        protected double minValue;
        protected double maxValue;

        protected double[,] real;
        protected double[,] source;
        protected bool[,] mask;
        protected double[] userAvg;
        protected double[] serviceAvg;

        // Precomputed.
        protected double[,] userSim;
        protected double[,] serviceSim;
        protected List<int>[] similarUsers;
        protected List<int>[] similarServices;

        protected Random r = new Random();

        public CFPrediction(double[,] source, bool[,] mask, double[,] real,
            int numUsers, int numServices, double minValue, double maxValue,
            int topkUsers = 0, int topkServices = 0, double lambda = -1)
        {
            this.real = real;
            this.source = source;
            this.mask = mask;
            U_RANK = numUsers;
            S_RANK = numServices;
            this.minValue = minValue;
            this.maxValue = maxValue;
            this.topkUsers = topkUsers;
            this.topkServices = topkServices;
            this.lambda = lambda;

            userAvg = new double[U_RANK];
            serviceAvg = new double[S_RANK];
            userSim = new double[U_RANK, U_RANK];
            serviceSim = new double[S_RANK, S_RANK];
            similarUsers = new List<int>[U_RANK];
            similarServices = new List<int>[S_RANK];

            // Precompute the averages.
            for (int user = 0; user < U_RANK; ++user)
                userAvg[user] = averageUserAvailability(user);

            for (int service = 0; service < S_RANK; ++service)
                serviceAvg[service] = averageServiceAvailability(service);
        }

        protected virtual void precomputeSimilarities()
        {
            for (int u1 = 0; u1 < U_RANK; ++u1)
            {
                userSim[u1, u1] = 1;

                for (int u2 = u1 + 1; u2 < U_RANK; ++u2)
                    updateSimilarity(userSim, u1, u2, userSimilaritySums(u1, u2));
            }

            for (int s1 = 0; s1 < S_RANK; ++s1)
            {
                serviceSim[s1, s1] = 1;

                for (int s2 = s1 + 1; s2 < S_RANK; ++s2)
                    updateSimilarity(serviceSim, s1, s2, serviceSimilaritySums(s1, s2));
            }
        }

        protected void precomputeSimilar()
        {
            for (int user = 0; user < U_RANK; ++user)
                similarUsers[user] = mostSimilar(user, userSim, U_RANK);

            for (int service = 0; service < S_RANK; ++service)
                similarServices[service] = mostSimilar(service, serviceSim, S_RANK);
        }

        // Returns sorted users/services with positive similarity to curr.
        protected List<int> mostSimilar(int curr, double[,] sim, int rank)
        {
            List<int> candidates = new List<int>();
            for (int i = 0; i < rank; ++i)
                if (sim[curr, i] > 0)
                    candidates.Add(i);
            return candidates.OrderByDescending(i => sim[curr, i]).ToList();
        }

        protected void updateSimilarity(double[,] sim, int x, int y,
            Tuple<double, double, double> similaritySums, List<int>[] similarItems = null)
        {
            double exSim = sim[x, y];
            double sum1 = similaritySums.Item1;
            double sum2 = similaritySums.Item2;
            double sum3 = similaritySums.Item3;
            sim[x, y] = sim[y, x] =
                ((sum2 > 0 && sum3 > 0) ? sum1 / (Math.Sqrt(sum2) * Math.Sqrt(sum3)) : -1);
            
            return;
            if (similarItems == null)
                return;
            if (exSim <= 0 && sim[x, y] > 0)
            {
                similarItems[x].Add(y);
                similarItems[y].Add(x);
            }
            if (exSim > 0 && sim[x, y] <= 0)
            {
                similarItems[x].Remove(y);
                similarItems[y].Remove(x);
            }
        }

        protected Tuple<double, double, double> serviceSimilaritySums(int s1, int s2)
        {
            double sum1 = 0;
            double sum2 = 0;
            double sum3 = 0;
            for (int user = 0; user < U_RANK; user++)
                if (mask[user, s1] && mask[user, s2])
                {
                    double a = source[user, s1] - serviceAvg[s1];
                    double b = source[user, s2] - serviceAvg[s2];
                    sum1 += a * b;
                    sum2 += a * a;
                    sum3 += b * b;
                }
            return new Tuple<double, double, double>(sum1, sum2, sum3);
        }

        protected Tuple<double, double, double> userSimilaritySums(int u1, int u2)
        {
            double sum1 = 0;
            double sum2 = 0;
            double sum3 = 0;
            for (int service = 0; service < S_RANK; service++)
                if (mask[u1, service] && mask[u2, service])
                {
                    double a = source[u1, service] - userAvg[u1];
                    double b = source[u2, service] - userAvg[u2];
                    sum1 += a * b;
                    sum2 += a * a;
                    sum3 += b * b;
                }
            return new Tuple<double, double, double>(sum1, sum2, sum3);
        }

        protected double averageServiceAvailability(int service)
        {
            int count = 0;
            double aggregate = 0;
            for (int user = 0; user < U_RANK; user++)
            {
                if (mask[user, service])
                {
                    aggregate += source[user, service];
                    count++;
                }
            }
            return (count != 0) ? aggregate / count : -1;
        }

        protected double averageUserAvailability(int user)
        {
            int count = 0;
            double aggregate = 0;
            for (int service = 0; service < S_RANK; service++)
            {
                if (mask[user, service])
                {
                    aggregate += source[user, service];
                    count++;
                }
            }
            return (count != 0) ? aggregate / count : -1;
        }

        public Tuple<double, double> calculateRMSEandMAE(List<Tuple<int,int> > testingSet, TextWriter file = null)
        {
            int count = 0;
            double mae = 0;
            double rmse = 0;
            foreach (Tuple<int, int> random in testingSet)
            {
                double cf = predictionInPoint(random.Item1, random.Item2);
                mae += Math.Abs(cf - real[random.Item1, random.Item2]);
                rmse += Math.Pow(cf - real[random.Item1, random.Item2], 2);
                count++;

                //if (file != null)
                //    file.WriteLine("real = {0}, prediction = {1}", real[random.Item1, random.Item2], cf);
            }
            return new Tuple<double, double>(Math.Sqrt(rmse / count), mae / count);
        }

        public Tuple<double, double> calculateRMSPEandMAPE(List<Tuple<int, int>> testingSet)
        {
            int count = 0;
            double mape = 0;
            double rmspe = 0;
            foreach (Tuple<int, int> random in testingSet)
            {
                double error = (predictionInPoint(random.Item1, random.Item2) - real[random.Item1, random.Item2])
                    / real[random.Item1, random.Item2];
                mape += Math.Abs(error);
                rmspe += Math.Pow(error, 2);
                count++;
            }
            return new Tuple<double, double>(Math.Sqrt(rmspe / count), mape / count);
        }

        protected virtual double calculateUsersImpact(int u, int s) { return 0; }
        protected virtual double calculateServicesImpact(int u, int s) { return 0; }

        protected double lambdaPrediction(double upcc, double ipcc)
        {
            if (ipcc == -1 && upcc == -1) return 0;
            if (ipcc == -1) return upcc;
            if (upcc == -1) return ipcc;
            if (lambda == -1) return (upcc + ipcc) / 2;
            return lambda * upcc + (1 - lambda) * ipcc;
        }
    }
}
