﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections;
using System.Diagnostics;

namespace TS2Eval
{
    class SimilarPairsBasedPrediction : CFPrediction
    {
        double[,] numSamples;
        int stohastic;  // 0 = ne uzima u obzir broj sampleova, 1 = uzima, 2 = uzima s obzirom na vrijeme
        Tuple<double, double, double>[,] userSimSums;
        Tuple<double, double, double>[,] serviceSimSums;
        double sampleWeight = 1;
        double sampleWeightIncrement = 0;
        bool approx;

        public SimilarPairsBasedPrediction(double[,] source, bool[,] mask, int[,] numSamples, double[,] real,
                                           int numUsers, int numServices, double minValue, double maxValue,
                                           int topkUsers, int topKServices, double lambda, int stohastic = 0, bool approx = true)
            : base(source, mask, real, numUsers, numServices,  minValue, maxValue, topkUsers, topKServices, lambda)
        {
            this.approx = approx;
            this.stohastic = stohastic;
            if (stohastic > 0)
            {
                this.numSamples = new double[U_RANK, S_RANK];
                for (int u = 0; u < U_RANK; ++u)
                    for (int s = 0; s < S_RANK; ++s)
                        this.numSamples[u, s] = (double)numSamples[u, s];
                if (stohastic == 2)
                    sampleWeightIncrement = 0.01;

                userSimSums = new Tuple<double, double, double>[U_RANK, U_RANK];
                serviceSimSums = new Tuple<double, double, double>[S_RANK, S_RANK];
            }
            precomputeSimilarities();
            precomputeSimilar();
        }

        protected override void precomputeSimilarities()
        {
            if (!approx)
            {
                base.precomputeSimilarities();
                return;
            }

            Matrix M = new Matrix(U_RANK, S_RANK);
            Matrix U = new Matrix(U_RANK, S_RANK);
            Matrix UU = new Matrix(U_RANK, S_RANK);
            Matrix S = new Matrix(U_RANK, S_RANK);
            Matrix SSt = new Matrix(S_RANK, U_RANK);

            for (int u = 0; u < U_RANK; ++u)
                for (int s = 0; s < S_RANK; ++s)
                {
                    if (!mask[u, s]) continue;
                    M[u, s] = 1;
                    U[u, s] = source[u, s] - userAvg[u];
                    UU[u, s] = (source[u, s] - userAvg[u]) * (source[u, s] - userAvg[u]);
                    S[u, s] = source[u, s] - serviceAvg[s];
                    SSt[s, u] = (source[u, s] - serviceAvg[s]) * (source[u, s] - serviceAvg[s]);
                }

            Stopwatch sw = new Stopwatch();
            sw.Start();
            Matrix U1 = Matrix.ApproxMultiply(U, U, 1);
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds / 1000.0);
            sw.Restart();
            Matrix U2 = Matrix.ApproxMultiply(UU, M, 3);
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds / 1000.0);

            sw.Restart();
            Matrix S1 = Matrix.ApproxMultiply(S, S, 2);
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds / 1000.0);
            sw.Restart();
            Matrix S2 = Matrix.ApproxMultiply(SSt, M);
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds / 1000.0);

            for (int u1 = 0; u1 < U_RANK; ++u1)
            {
                userSim[u1, u1] = 1;

                for (int u2 = u1 + 1; u2 < U_RANK; ++u2)
                {
                    updateSimilarity(userSim, u1, u2,
                        new Tuple<double, double, double>(U1[u1, u2], U2[u1, u2], U2[u2, u1]));
                    if (stohastic > 0)
                    {
                        userSimSums[u1, u2] = new Tuple<double, double, double>(U1[u1, u2], U2[u1, u2], U2[u2, u1]);
                        userSimSums[u2, u1] = new Tuple<double, double, double>(U1[u1, u2], U2[u2, u1], U2[u1, u2]);
                    }
                }
            }

            for (int s1 = 0; s1 < S_RANK; ++s1)
            {
                serviceSim[s1, s1] = 1;

                for (int s2 = s1 + 1; s2 < S_RANK; ++s2)
                {
                    updateSimilarity(serviceSim, s1, s2,
                        new Tuple<double, double, double>(S1[s1, s2], S2[s1, s2], S2[s2, s1]));
                    if (stohastic > 0) {
                        serviceSimSums[s1, s2] = new Tuple<double, double, double>(S1[s1, s2], S2[s1, s2], S2[s2, s1]);
                        serviceSimSums[s2, s1] = new Tuple<double, double, double>(S1[s1, s2], S2[s2, s1], S2[s1, s2]);
                    }
                }
            }

            U1 = U2 = S1 = S2 = M = U = UU = S = SSt = null;
        }

        protected Tuple<double, double> servicesImpact(int user, int currentService)
        {
            double currentAverage = serviceAvg[currentService];
            double impact = 0;
            double totalWeight = 0;
            int count = 0;
            foreach (int service in similarServices[currentService])
                if (mask[user, service])
                {
                    double weight = serviceSim[currentService, service];
                    if (stohastic > 0) weight *= numSamples[user, service];
                    totalWeight += weight;
                    impact += weight * source[user, service]
                            * (serviceAvg[service] > 0 ? serviceAvg[currentService] / serviceAvg[service] : 1);
                    if (++count == topkServices) break;
                }
            return new Tuple<double, double>(impact, totalWeight);
        }

        protected Tuple<double, double> usersImpact(int currentUser, int service)
        {
            double currentAverage = userAvg[currentUser];
            double impact = 0;
            double totalWeight = 0;
            int count = 0;
            foreach (int user in similarUsers[currentUser])
                if (mask[user, service])
                {
                    double weight = userSim[currentUser, user];
                    if (stohastic > 0) weight *= numSamples[user, service];
                    totalWeight += weight;
                    impact += weight * source[user, service]
                            * (userAvg[user] > 0 ? userAvg[currentUser] / userAvg[user] : 1);
                    if (++count == topkUsers) break;
                }
            return new Tuple<double, double>(impact, totalWeight);
        }

        public override double predictionInPoint(int u, int s)
        {
            if (mask[u, s] && stohastic == 0)
                return source[u, s];

            Tuple<double, double> up = usersImpact(u, s);
            Tuple<double, double> ip = servicesImpact(u, s);

            if (lambda == -1 && ip.Item2 + up.Item2 > 0)
                return (ip.Item1 + up.Item1) / (ip.Item2 + up.Item2);

            double upcc = (up.Item2 > 0 ? up.Item1 / up.Item2 : userAvg[u]);
            double ipcc = (ip.Item2 > 0 ? ip.Item1 / ip.Item2 : serviceAvg[s]);

            double ret = lambdaPrediction(upcc, ipcc);
            return Math.Max(Math.Min(ret, maxValue), minValue);
        }

        private void updateSimilaritySums(Tuple<double, double, double>[,] simSums,
            int x, int y, double a, double b, bool minus = false)
        {
            int coeff = (minus ? -1 : 1);
            double sum1 = simSums[x, y].Item1 + coeff * a * b;
            double sum2 = simSums[x, y].Item2 + coeff * a * a;
            double sum3 = simSums[x, y].Item3 + coeff * b * b;
            simSums[x, y] = new Tuple<double, double, double>(sum1, sum2, sum3);
            simSums[y, x] = new Tuple<double, double, double>(sum1, sum3, sum2);
        }


        // Najkompliciranija funkcija:
        // azurira vrijednosti, prosjeke i slicnosti kad stigne novi zapis.

        public void update(int currentUser, int currentService, int success)
        {
            // In the case of updating an existing field, first reduce the similarity sums.
            if (mask[currentUser, currentService])
            {
                for (int user = 0; user < U_RANK; ++user)
                {
                    if (user == currentUser || !mask[user, currentService])
                        continue;

                    double a = source[currentUser, currentService] - userAvg[currentUser];
                    double b = source[user, currentService] - userAvg[user];

                    updateSimilaritySums(userSimSums, currentUser, user, a, b, true);
                }

                for (int service = 0; service < S_RANK; ++service)
                {
                    if (service == currentService || !mask[currentUser, service])
                        continue;

                    double a = source[currentUser, currentService] - serviceAvg[currentService];
                    double b = source[currentUser, service] - serviceAvg[service];

                    updateSimilaritySums(serviceSimSums, currentService, service, a, b, true);
                }
            }
            mask[currentUser, currentService] = true;

            // Update the main values.
            source[currentUser, currentService] =
                (source[currentUser, currentService] * numSamples[currentUser, currentService] + sampleWeight * success)
                / (numSamples[currentUser, currentService] + sampleWeight);
            numSamples[currentUser, currentService] += sampleWeight;
            sampleWeight += sampleWeightIncrement;

            // Update the averages.
            userAvg[currentUser] = averageUserAvailability(currentUser);
            serviceAvg[currentService] = averageServiceAvailability(currentService);

            // Add to the similarities.

            for (int user = 0; user < U_RANK; ++user)
            {
                if (user == currentUser || !mask[user, currentService])
                    continue;

                double a = source[currentUser, currentService] - userAvg[currentUser];
                double b = source[user, currentService] - userAvg[user];

                updateSimilaritySums(userSimSums, currentUser, user, a, b);
                updateSimilarity(userSim, currentUser, user,
                    userSimSums[currentUser, user], similarUsers);
            }

            for (int service = 0; service < S_RANK; ++service)
            {
                if (service == currentService || !mask[currentUser, service])
                    continue;

                double a = source[currentUser, currentService] - serviceAvg[currentService];
                double b = source[currentUser, service] - serviceAvg[service];

                updateSimilaritySums(serviceSimSums, currentService, service, a, b);
                updateSimilarity(serviceSim, currentService, service,
                    serviceSimSums[currentService, service], similarServices);
            }
        }
    }
}
